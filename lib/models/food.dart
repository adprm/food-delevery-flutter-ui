class Food {
  String imgUrl;
  String desc;
  String name;
  String waitTime;
  num score;
  String cal;
  num price;
  num quantity;
  List<Map<String, String>> ingredients;
  String about;
  bool hightLight;
  Food(this.imgUrl, this.desc, this.name, this.waitTime, this.score, this.cal,
      this.price, this.quantity, this.ingredients, this.about,
      {this.hightLight = false});
  static List<Food> generateRecomendFoods() {
    return [
      Food(
          'assets/images/dish1.png',
          'No1. in sales',
          'Soba soup',
          '50 min',
          4.8,
          '324 kcal',
          12,
          1,
          [
            {'Noodle': 'assets/images/ingre1.png'},
            {'Shrimp': 'assets/images/ingre2.png'},
            {'Egg': 'assets/images/ingre3.png'},
          ],
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum metus libero, porta ac posuere a, porttitor et erat. Sed viverra neque ac dui tincidunt tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer elementum tortor leo, in fringilla ipsum lobortis a. Sed imperdiet imperdiet iaculis. Nunc vitae vestibulum tortor. Sed rutrum orci nec erat tincidunt, in porta mi faucibus. Nullam sed condimentum nunc, ac imperdiet diam. Suspendisse eu felis in tellus ultricies tristique. Etiam mattis hendrerit malesuada.',
          hightLight: true),
      Food(
          'assets/images/dish2.png',
          'No1. in sales',
          'Soto',
          '50 min',
          4.8,
          '324 kcal',
          12,
          1,
          [
            {'Noodle': 'assets/images/ingre1.png'},
            {'Shrimp': 'assets/images/ingre2.png'},
            {'Egg': 'assets/images/ingre3.png'},
          ],
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum metus libero, porta ac posuere a, porttitor et erat. Sed viverra neque ac dui tincidunt tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer elementum tortor leo, in fringilla ipsum lobortis a. Sed imperdiet imperdiet iaculis. Nunc vitae vestibulum tortor. Sed rutrum orci nec erat tincidunt, in porta mi faucibus. Nullam sed condimentum nunc, ac imperdiet diam. Suspendisse eu felis in tellus ultricies tristique. Etiam mattis hendrerit malesuada.'),
      Food(
          'assets/images/dish3.png',
          'No1. in sales',
          'Bakso',
          '50 min',
          4.8,
          '324 kcal',
          12,
          1,
          [
            {'Noodle': 'assets/images/ingre1.png'},
            {'Shrimp': 'assets/images/ingre2.png'},
            {'Egg': 'assets/images/ingre3.png'},
          ],
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum metus libero, porta ac posuere a, porttitor et erat. Sed viverra neque ac dui tincidunt tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer elementum tortor leo, in fringilla ipsum lobortis a. Sed imperdiet imperdiet iaculis. Nunc vitae vestibulum tortor. Sed rutrum orci nec erat tincidunt, in porta mi faucibus. Nullam sed condimentum nunc, ac imperdiet diam. Suspendisse eu felis in tellus ultricies tristique. Etiam mattis hendrerit malesuada.'),
      Food(
          'assets/images/dish4.png',
          'This one',
          'Mie Ayam',
          '50 min',
          4.8,
          '324 kcal',
          12,
          1,
          [
            {'Noodle': 'assets/images/ingre1.png'},
            {'Shrimp': 'assets/images/ingre2.png'},
            {'Egg': 'assets/images/ingre3.png'},
          ],
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum metus libero, porta ac posuere a, porttitor et erat. Sed viverra neque ac dui tincidunt tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer elementum tortor leo, in fringilla ipsum lobortis a. Sed imperdiet imperdiet iaculis. Nunc vitae vestibulum tortor. Sed rutrum orci nec erat tincidunt, in porta mi faucibus. Nullam sed condimentum nunc, ac imperdiet diam. Suspendisse eu felis in tellus ultricies tristique. Etiam mattis hendrerit malesuada.')
    ];
  }

  static List<Food> generatePopularFoods() {
    return [
      Food(
          'assets/images/dish3.png',
          'No1. in sales',
          'Ayam Bakar',
          '50 min',
          4.8,
          '324 kcal',
          12,
          1,
          [
            {'Noodle': 'assets/images/ingre1.png'},
            {'Shrimp': 'assets/images/ingre2.png'},
            {'Egg': 'assets/images/ingre3.png'},
          ],
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum metus libero, porta ac posuere a, porttitor et erat. Sed viverra neque ac dui tincidunt tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer elementum tortor leo, in fringilla ipsum lobortis a. Sed imperdiet imperdiet iaculis. Nunc vitae vestibulum tortor. Sed rutrum orci nec erat tincidunt, in porta mi faucibus. Nullam sed condimentum nunc, ac imperdiet diam. Suspendisse eu felis in tellus ultricies tristique. Etiam mattis hendrerit malesuada.'),
      Food(
          'assets/images/dish4.png',
          'No1. in sales',
          'Teriyaki',
          '50 min',
          4.8,
          '324 kcal',
          12,
          1,
          [
            {'Noodle': 'assets/images/ingre1.png'},
            {'Shrimp': 'assets/images/ingre2.png'},
            {'Egg': 'assets/images/ingre3.png'},
          ],
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum metus libero, porta ac posuere a, porttitor et erat. Sed viverra neque ac dui tincidunt tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer elementum tortor leo, in fringilla ipsum lobortis a. Sed imperdiet imperdiet iaculis. Nunc vitae vestibulum tortor. Sed rutrum orci nec erat tincidunt, in porta mi faucibus. Nullam sed condimentum nunc, ac imperdiet diam. Suspendisse eu felis in tellus ultricies tristique. Etiam mattis hendrerit malesuada.',
          hightLight: true)
    ];
  }
}
